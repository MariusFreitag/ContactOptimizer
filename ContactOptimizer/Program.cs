﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ContactOptimizer
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputFile = "input.csv";
            string outputFile = "output.csv";
            char separator = ',';

            CsvData csvData = CsvData.FromFile(inputFile, separator);

            csvData.ForEach((item) =>
            {
                var contact = new Contact(item);
                contact.Format();

                if (!contact.HasNormalTypes())
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(contact.FullName + " has got unusual Types!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine(item.ToString('.'));
                }
            });

            csvData.ToFile(outputFile, separator);

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Finished");
            Console.ReadKey();
        }
    }
}