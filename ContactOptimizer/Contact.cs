﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ContactOptimizer
{
    class Contact
    {
        public string GivenName
        {
            get
            {
                return _["Given Name"];
            }
            set
            {
                _["Given Name"] = value;
            }
        }
        public string AdditionalName
        {
            get
            {
                return _["Additional Name"];
            }
            set
            {
                _["Additional Name"] = value;
            }
        }
        public string FamilyName
        {
            get
            {
                return _["Family Name"];
            }
            set
            {
                _["Family Name"] = value;
            }
        }
        public string FullName
        {
            get
            {
                return _["Name"];
            }
            set
            {
                _["Name"] = value;
            }
        }

        public List<IContactInfo> AdditionalInformation { get; set; } = new List<IContactInfo>();

        private CsvItem _;

        public Contact(CsvItem csvItem)
        {
            _ = csvItem;

            #region Add addresses
            for (int i = 1; true; i++)
            {
                try
                {
                    var address = new Address(csvItem, i);

                    if (!address.IsEmpty())
                    {
                        AdditionalInformation.Add(address);
                    }
                }
                catch
                {
                    break;
                }
            }
            #endregion

            #region Add phone numbers
            for (int i = 1; true; i++)
            {
                try
                {
                    var phoneNumber = new PhoneNumber(csvItem, i);

                    if (!phoneNumber.IsEmpty())
                    {
                        AdditionalInformation.Add(phoneNumber);
                    }
                }
                catch
                {
                    break;
                }
            }
            #endregion

            #region Add email adresses
            for (int i = 1; true; i++)
            {
                try
                {
                    var emailAddress = new EmailAddress(csvItem, i);

                    if (!emailAddress.IsEmpty())
                    {
                        AdditionalInformation.Add(emailAddress);
                    }
                }
                catch
                {
                    break;
                }
            }
            #endregion
        }

        public void Format()
        {
            if (AdditionalName == "")
            {
                FullName = GivenName + " " + FamilyName;
            }
            else
            {

                FullName = GivenName + "" + AdditionalName + " " + FamilyName;
            }

            AdditionalInformation.ForEach((x) => x.Format());
        }

        public bool HasNormalTypes()
        {
            foreach (var info in AdditionalInformation)
            {
                if (!info.HasNormalType())
                {
                    return false;
                }
            }

            return true;
        }
    }

    interface IContactInfo
    {
        bool IsEmpty();
        bool HasNormalType();
        void Format();
    }

    class Address : IContactInfo
    {
        public string Type
        {
            get
            {
                return _[$"Address {_id} - Type"];
            }
            set
            {
                _[$"Address {_id} - Type"] = value;
            }
        }
        public string Street
        {
            get
            {
                return _[$"Address {_id} - Street"];
            }
            set
            {
                _[$"Address {_id} - Street"] = value;
            }
        }
        public string PostalCode
        {
            get
            {
                return _[$"Address {_id} - Postal Code"];
            }
            set
            {
                _[$"Address {_id} - Postal Code"] = value;
            }
        }
        public string City
        {
            get
            {
                return _[$"Address {_id} - City"];
            }
            set
            {
                _[$"Address {_id} - City"] = value;
            }
        }
        public string Region
        {
            get
            {
                return _[$"Address {_id} - Region"];
            }
            set
            {
                _[$"Address {_id} - Region"] = value;
            }
        }
        public string Country
        {
            get
            {
                return _[$"Address {_id} - Country"];
            }
            set
            {
                _[$"Address {_id} - Country"] = value;
            }
        }
        public string FormattedAddress
        {
            get
            {
                return _[$"Address {_id} - Formatted"];
            }
            set
            {
                _[$"Address {_id} - Formatted"] = value;
            }
        }

        private int _id;
        private CsvItem _;

        public Address(CsvItem csvItem, int id)
        {
            _ = csvItem;
            _id = id;
        }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(Type);
        }

        public bool HasNormalType()
        {
            return Type == "Home";
        }

        public void Format()
        {
            // If city is empty, move region to city
            // Google sometimes fails in this
            if (City == "")
            {
                City = Region;
                Region = "";
            }

            #region Format Address
            bool hasStreet = Street != "";
            bool hasCity = PostalCode != "" || City != "";
            bool hasCountry = Country != "";

            if (hasStreet && hasCity && hasCountry)
            {
                FormattedAddress = $"\"{Street}; {PostalCode} {City}; {Country}\"";
            }
            else if (hasStreet && hasCity)
            {
                FormattedAddress = $"\"{Street}; {PostalCode} {City}\"";
            }
            else if (hasCity && hasCountry)
            {
                FormattedAddress = $"\"{PostalCode} {City}; {Country}\"";
            }
            else if (hasStreet && hasCountry)
            {
                FormattedAddress = $"\"{Street}; {Country}\"";
            }
            else if (hasStreet)
            {
                FormattedAddress = $"\"{Street}\"";
            }
            else if (hasCity)
            {
                FormattedAddress = $"\"{PostalCode} {City}\"";
            }
            #endregion
        }
    }

    class PhoneNumber : IContactInfo
    {
        public string Type
        {
            get
            {
                return _[$"Phone {_id} - Type"];
            }
            set
            {
                _[$"Phone {_id} - Type"] = value;
            }
        }
        public string Number
        {
            get
            {
                return _[$"Phone {_id} - Value"];
            }
            set
            {
                _[$"Phone {_id} - Value"] = value;
            }
        }

        private int _id;
        private CsvItem _;

        public PhoneNumber(CsvItem csvItem, int id)
        {
            _ = csvItem;
            _id = id;
        }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(Type);
        }

        public bool HasNormalType()
        {
            bool isMobile = Number.Substring(4).StartsWith("1");
            return (Type == "Home" && !isMobile)
                || (Type == "Mobile" && isMobile)
                || Type == "Work";
        }

        public void Format()
        {
            #region Format country code
            // Replace non-numeric values
            Number = new Regex("[^0-9]").Replace(Number, "");

            if (Number.StartsWith("00"))
            {
                Number = Number.Substring(2);
            }
            else if (Number.StartsWith("0"))
            {
                Number = "49" + Number.Substring(1);
            }

            #endregion

            #region Set Whitespaces
            // If is mobile
            if (Number.Substring(2).StartsWith("1"))
            {
                Number = "+"
                    + Number.Substring(0, 2) + " " // Country Code
                    + Number.Substring(2, 3) + " " // Pre Code
                    + Number.Substring(5, Number.Length - 5) // After Code
                    ;
            }
            else
            {
                Number = "+"
                    + Number.Substring(0, 2) + " " // Country Code
                    + Number.Substring(2, 4) + " " // Pre Code
                    + Number.Substring(6, Number.Length - 6) // After Code
                    ;
            }
            #endregion
        }
    }

    class EmailAddress : IContactInfo
    {
        public string Type
        {
            get
            {
                return _[$"E-mail {_id} - Type"];
            }
            set
            {
                _[$"E-mail {_id} - Type"] = value;
            }
        }
        public string Address
        {
            get
            {
                return _[$"E-mail {_id} - Value"];
            }
            set
            {
                _[$"E-mail {_id} - Value"] = value;
            }
        }

        private int _id;
        private CsvItem _;

        public EmailAddress(CsvItem csvItem, int id)
        {
            _ = csvItem;
            _id = id;
        }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(Type);
        }

        public bool HasNormalType()
        {
            return Type == "Home"
                || Type == "Work";
        }

        public void Format()
        {
            // Do nothing
        }
    }
}
