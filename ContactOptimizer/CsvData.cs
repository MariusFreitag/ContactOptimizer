﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactOptimizer
{
    class CsvData
    {
        public CsvItem this[int index]
        {
            get
            {
                if (index >= _data.Length)
                {
                    throw new Exception($"The CSV date item '{index}' does not exist!");
                }
                return _data[index];
            }
        }

        private List<string> _indexes = new List<string>();
        private CsvItem[] _data;

        private CsvData(string[] inputLines, char separator)
        {
            if (inputLines.Length == 0)
            {
                throw new Exception("The CSV must have a header!");
            }

            _indexes = new List<string>(inputLines[0].Split(separator));
            _data = new CsvItem[inputLines.Length - 1];

            for (int i = 1; i <= inputLines.Length - 1; i++)
            {
                _data[i - 1] = new CsvItem(_indexes, inputLines[i].Split(separator));
            }
        }

        public void ToFile(string path, char separator)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.WriteAllText(path, string.Join(separator.ToString(), _indexes) + Environment.NewLine);

            foreach (CsvItem item in _data)
            {
                File.AppendAllText(path, item.ToString(separator) + Environment.NewLine);
            }
        }

        public static CsvData FromFile(string path, char separator)
        {
            return new CsvData(File.ReadAllLines(path), separator);
        }

        public void ForEach(Action<CsvItem> action)
        {
            foreach (var item in _data)
            {
                action(item);
            }
        }
    }

    class CsvItem
    {
        private List<string> _indexes = new List<string>();
        private string[] _data;

        public CsvItem(List<string> indexes, string[] data)
        {
            _indexes = indexes;
            _data = data;

            if (_indexes.Count != data.Length)
            {
                throw new Exception("The amount of data values does not match the index count!");
            }
        }

        public string this[string index]
        {
            get
            {
                if (!_indexes.Contains(index))
                {
                    throw new Exception($"The CSV index '{index}' does not exist!");
                }
                return _data[_indexes.IndexOf(index)];
            }
            set
            {
                if (!_indexes.Contains(index))
                {
                    throw new Exception($"The CSV index '{index}' does not exist!");
                }
                _data[_indexes.IndexOf(index)] = value;
            }
        }

        public string ToString(char separator)
        {
            return string.Join(separator.ToString(), _data);
        }
    }
}
